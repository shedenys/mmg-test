<?php

namespace App\Observers;

use App\Models\Db\Post;
use Storage;

/**
 * Class PostObserver
 * @package App\Observers
 */
class PostObserver
{
    /**
     * Handle the post "updated" event.
     * @param Post $post
     * @return void
     */
    public function updated(Post $post): void
    {
        // Deleting previous file, if it has been changed
        $previousFile = $post->getOriginal('file');
        if ($previousFile !== $post->file) {
            Storage::delete($previousFile);
        }
    }

    /**
     * Handle the post "deleted" event.
     * @param Post $post
     * @return void
     */
    public function deleted(Post $post)
    {
        // Delete file
        Storage::delete($post->file);
    }
}
