<?php

namespace App\Repositories;

use App\Models\Db\Session;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Jenssegers\Agent\Agent;

/**
 * User statistics repository
 * Class IncomingUserRepository
 * @package App\Repositories
 */
class UserRepository
{
    /**
     * Get stored statistics
     * @return Collection
     */
    public function getStatistics(): Collection
    {
        return Session::select(['user_agent', DB::raw('count(*) as total')])->groupBy('user_agent')
            ->get();
    }

    /**
     * Check, if current user is stored in statistics
     * @return bool
     */
    public function isCurrentUserSaved(): bool
    {
        $agent = new Agent();

        return (bool) Session::where('user_agent', $agent->getUserAgent())
            ->where('ip_address', \Request::ip())
            ->count();
    }
}
