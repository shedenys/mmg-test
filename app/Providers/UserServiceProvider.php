<?php

namespace App\Providers;

use App\Services\User\Contracts\UserInterface;
use App\Services\User\UserService;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(UserInterface::class, UserService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [UserInterface::class];
    }
}
