<?php

namespace App\Providers;

use App\Services\PageMeta\Contracts\PageMetaInterface;
use App\Services\PageMeta\PageMetaService;
use Illuminate\Support\ServiceProvider;

/**
 * Class PageMetaServiceProvider
 * @package App\Providers
 */
class PageMetaServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function register(): void
    {
        $this->app->singleton(PageMetaInterface::class, PageMetaService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [PageMetaInterface::class];
    }
}
