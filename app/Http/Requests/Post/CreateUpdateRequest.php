<?php

namespace App\Http\Requests\Post;

use App\Models\Db\Post;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Post create and update validation
 * Class CreateUpdateRequest
 * @package App\Http\Requests\Post
 */
class CreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required|integer',
            'name' => 'required|string',
            'content' => 'required|string',
            'file' => 'file|max:' . Post::MAX_FILE_SIZE_KB,
        ];
    }
}
