<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCommentRequest;
use App\Http\Requests\Category\CreateUpdateRequest;
use App\Models\Db\Category;
use App\Models\Db\CategoryComment;
use App\Services\Category\CategoryService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use PageMeta;
use Session;

/**
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * CategoryController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of categories.
     * @return View
     */
    public function index(): View
    {
        PageMeta::setTitle(__('category.categories'));

        return view('category.index', [
            'categories' => $this->categoryService->getPaginated()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return View
     */
    public function create(): View
    {
        PageMeta::setTitle(__('category.create.title'));
        Session::put(self::REDIRECT_TO, redirect()->back()->getTargetUrl());

        return view('category.create', [
            'category' => new Category(),
        ]);
    }

    /**
     * Store a newly created category in storage.
     * @param CreateUpdateRequest $request
     * @return array
     */
    public function store(CreateUpdateRequest $request): array
    {
        $this->categoryService->create($request->except('_token'));
        $request->session()->flash('success-title', __('common.messages.create.success.title'));
        $request->session()->flash('success', __('category.messages.create.success'));

        return ['redirect' => Session::pull(self::REDIRECT_TO)];
    }

    /**
     * Display the specified category.
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show(Category $category): View
    {
        return view('category.show', [
            'category' => $category,
            'posts' => $category->posts()->paginate(),
            'comments' => $category->comments,
            'comment' => new CategoryComment()
        ]);
    }

    /**
     * Show the form for editing the specified category.
     * @param Category $category
     * @return View
     */
    public function edit(Category $category): View
    {
        Session::put(self::REDIRECT_TO, redirect()->back()->getTargetUrl());

        return view('category.edit', [
            'category' => $category,
        ]);
    }

    /**
     * Update the specified category in storage.
     * @param CreateUpdateRequest $request
     * @param Category $category
     * @return array
     */
    public function update(CreateUpdateRequest $request, Category $category): array
    {
        $category->update($request->except('_token'));
        $request->session()->flash('success-title', __('common.messages.update.success.title'));
        $request->session()->flash('success', __('category.messages.update.success'));

        return ['redirect' => Session::pull(self::REDIRECT_TO)];
    }

    /**
     * Remove the specified category from storage.
     * @param Category $category
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Category $category): RedirectResponse
    {
        $category->delete();

        return redirect()->back()->with('success', __('category.messages.delete.success'))
            ->with('success-title', __('common.messages.delete.success.title'));
    }

    /**
     * Create comment for category
     * @param CreateCommentRequest $request
     * @return array
     */
    public function storeComment(CreateCommentRequest $request): array
    {
        return ['comment' => $this->categoryService->createComment($request->except('_token'))];
    }
}
