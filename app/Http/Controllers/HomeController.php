<?php

namespace App\Http\Controllers;

use App\Facades\PageMeta;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    public function index()
    {
        PageMeta::setTitle(__('home.index.title'));

        return view('home.index');
    }
}
