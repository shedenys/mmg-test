<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\CreateCommentRequest;
use App\Http\Requests\Post\CreateUpdateRequest;
use App\Models\Db\Post;
use App\Models\Db\PostComment;
use App\Services\Category\CategoryService;
use App\Services\Post\PostService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use PageMeta;
use Session;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * @var PostService
     */
    protected $postService;

    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * PostController constructor.
     * @param PostService $postService
     * @param CategoryService $categoryService
     */
    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of posts.
     * @return View
     */
    public function index(): View
    {
        PageMeta::setTitle(__('post.posts'));

        return view('post.index', [
            'posts' => $this->postService->getPaginated()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return View
     */
    public function create(): View
    {
        PageMeta::setTitle(__('post.create.title'));
        Session::put(self::REDIRECT_TO, redirect()->back()->getTargetUrl());

        return view('post.create', [
            'post' => new Post(),
            'categories' => $this->categoryService->getAllForSelect(),
        ]);
    }

    /**
     * Store a newly created post in storage.
     * @param CreateUpdateRequest $request
     * @return array
     */
    public function store(CreateUpdateRequest $request): array
    {
        $this->postService->create(array_merge(
            $request->except(['_token', 'file']),
            ['file' => $this->postService->storeFileFromRequest($request)]
        ));
        $request->session()->flash('success-title', __('common.messages.create.success.title'));
        $request->session()->flash('success', __('post.messages.create.success'));

        return ['redirect' => Session::pull(self::REDIRECT_TO)];
    }

    /**
     * Display the specified post.
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show(Post $post): View
    {
        return view('post.show', [
            'post' => $post,
            'comments' => $post->comments,
            'comment' => new PostComment()
        ]);
    }

    /**
     * Show the form for editing the specified post.
     * @param Post $post
     * @return View
     */
    public function edit(Post $post): View
    {
        Session::put(self::REDIRECT_TO, redirect()->back()->getTargetUrl());

        return view('post.edit', [
            'post' => $post,
            'categories' => $this->categoryService->getAllForSelect(),
        ]);
    }

    /**
     * Update the specified post in storage.
     * @param CreateUpdateRequest $request
     * @param Post $post
     * @return array
     */
    public function update(CreateUpdateRequest $request, Post $post): array
    {
        $this->postService->update(array_merge(
            $request->except(['_token', 'file']),
            ['file' => $this->postService->storeFileFromRequest($request)]
        ), $post);

        $request->session()->flash('success-title', __('common.messages.update.success.title'));
        $request->session()->flash('success', __('post.messages.update.success'));

        return ['redirect' => Session::pull(self::REDIRECT_TO)];
    }

    /**
     * Remove the specified category from storage.
     * @param Post $post
     * @throws \Exception
     * @return RedirectResponse
     */
    public function destroy(Post $post): RedirectResponse
    {
        $post->delete();

        return redirect()->back()->with('success', __('post.messages.delete.success'))
            ->with('success-title', __('common.messages.delete.success.title'));
    }

    /**
     * Download file
     * @param string $filename
     * @return BinaryFileResponse
     */
    public function downloadFile(string $filename): BinaryFileResponse
    {
        return response()->download(storage_path('app') . '/'.Post::STORE_PATH.'/'.$filename);
    }

    /**
     * Create comment for post
     * @param CreateCommentRequest $request
     * @return array
     */
    public function storeComment(CreateCommentRequest $request): array
    {
        return ['comment' => $this->postService->createComment($request->except('_token'))];
    }
}
