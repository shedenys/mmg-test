<?php

namespace App\Services\User\Contracts;

/**
 * Interface UserInterface
 * @package App\Services\User\Contracts
 */
interface UserInterface
{
    /**
     * Get browsers count statistics
     * @return array
     */
    public function getBrowserStatistics(): array;
}
