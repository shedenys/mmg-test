<?php

namespace App\Services\User;

use App\Repositories\UserRepository;
use Jenssegers\Agent\Agent;

/**
 * Class UserService
 * @package App\Services\User
 */
class UserService
{
    /**
     * Browser name
     * @var string
     */
    public $browser;

    /**
     * Browser users count
     * @var int
     */
    public $count;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * IncomingUserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get browsers count statistics
     */
    public function getBrowserStatistics(): array
    {
        // Get Stored statistics
        $statistics = $this->userRepository->getStatistics();

        $data = [];
        $currentUserAgent = new Agent();
        $agent = new Agent();

        // Check, if current user is stored in statistics
        $isCurrentUserSaved = $this->userRepository->isCurrentUserSaved();

        // Prepare statistics data grouped by browser and count for use
        foreach ($statistics as $item) {
            $agent->setUserAgent($item->user_agent);
            $userStatistics = new self($this->userRepository);
            $userStatistics->browser = $agent->browser();
            $userStatistics->count = $item->total;
            if (!$isCurrentUserSaved && ($userStatistics->browser === $currentUserAgent->browser())) {
                ++$userStatistics->count;
                $isCurrentUserSaved = true;
            }
            $data[] = $userStatistics;
        }
        // If statistics from storage is empty or current user not found in statistics
        if (!$statistics || !$isCurrentUserSaved) {
            $userStatistics = new self($this->userRepository);
            $userStatistics->browser = $currentUserAgent->browser();
            $userStatistics->count = 1;
            $data[] = $userStatistics;
        }

        return $data;
    }
}
