<?php

namespace App\Services\Category;

use App\Models\Db\Category;
use App\Models\Db\CategoryComment;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Business-logic of categories
 * Class CategoryService
 * @package App\Services
 */
class CategoryService
{
    /**
     * Get paginated lists of categories
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function getPaginated(int $perPage = null): LengthAwarePaginator
    {
        return Category::paginate($perPage);
    }

    /**
     * Category create
     * @param array $data
     */
    public function create(array $data): void
    {
        Category::create($data);
    }

     /**
     * Array of categories like ['id' => 'name'] for html-selects
     * @return array
     */
    public function getAllForSelect(): array
    {
        $categories = [];
        foreach (Category::all(['id', 'name']) as $category) {
            $categories[$category->id] = $category->name;
        }

        return $categories;
    }

    /**
     * Create comment for category
     * @param array $data
     * @return CategoryComment
     */
    public function createComment(array $data): CategoryComment
    {
        return CategoryComment::create($data);
    }
}
