<?php

namespace App\Services\Post;

use App\Models\Db\Post;
use App\Models\Db\PostComment;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Business-logic of posts
 * Class PostService
 * @package App\Services\Post
 */
class PostService
{
    /**
     * Get paginated lists of posts
     * @return LengthAwarePaginator
     */
    public function getPaginated(): LengthAwarePaginator
    {
        return Post::paginate();
    }

    /**
     * Create post
     * @param array $data
     */
    public function create(array $data): void
    {
        Post::create($data);
    }

    /**
     * Update post
     * @param array $data
     * @param Post $post
     */
    public function update(array $data, Post $post): void
    {
        if (!$data['file']) {
            unset($data['file']);
        }

        $post->update($data);
    }

    /**
     * Parse and store file in post from request
     * @param Request $request
     * @return null|string
     */
    public function storeFileFromRequest(Request $request): ?string
    {
        $file = $request->file('file');
        if ($file) {
            return $file->store(Post::STORE_PATH);
        }

        return null;
    }

    /**
     * Create comment for post
     * @param array $data
     * @return PostComment
     */
    public function createComment(array $data): PostComment
    {
        return PostComment::create($data);
    }
}
