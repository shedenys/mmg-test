<?php

namespace App\Services\PageMeta;

use App\Services\PageMeta\Contracts\PageMetaInterface;

/**
 * Page meta info
 * Class PageMeta
 * @package App\Services\PageMeta
 */
class PageMetaService implements PageMetaInterface
{
    /**
     * Page title
     * @var string
     */
    protected $title;

    /**
     * Page keywords
     * @var string
     */
    protected $keywords;

    /**
     * Page description
     * @var $description
     */
    protected $description;

    /**
     * Get page title
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set page title
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * Get page keywords
     * @return string|null
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * Set page keywords
     * @param string $keywords
     */
    public function setKeywords(string $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * Get page description
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set page description
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
