<?php

namespace App\Services\PageMeta\Contracts;

/**
 * Interface PageMetaInterface
 * @package App\Services\PageMeta\Contracts
 */
interface PageMetaInterface
{
    /**
     * Get page title
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Set page title
     * @param string $title
     * @return mixed
     */
    public function setTitle(string $title): void;

    /**
     * Get page keywords
     * @return string|null
     */
    public function getKeywords(): ?string;

    /**
     * Set page keywords
     * @param string $keywords
     */
    public function setKeywords(string $keywords): void;

    /**
     * Get page description
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * Set page description
     * @param string $description
     */
    public function setDescription(string $description): void;
}
