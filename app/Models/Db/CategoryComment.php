<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Db\CategoryComment
 *
 * @mixin \Eloquent
 */
class CategoryComment extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'category_id',
        'author',
        'content',
    ];

    /**
     * Relation to category
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
