<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Post
 *
 * @property int id
 * @property string file
 * @property mixed comments
 * @package App\Models\Db
 * @method static paginate(?int $perPage = null)
 * @method static create(array $data)
 * @mixin \Eloquent
 */
class Post extends Model
{
    public const CONTENT_PREVIEW_STR_LIMIT = 200;

    public const STORE_PATH = 'files';

    public const MAX_FILE_SIZE_KB = 2048;

    /**
     * @var array
     */
    protected $fillable = [
        'category_id',
        'name',
        'content',
        'file',
    ];

    /**
     * Relation to comments
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(PostComment::class);
    }

    /**
     * Relation to category
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
