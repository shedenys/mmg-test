<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Session driver model
 * Class Session
 *
 * @package App\Models\Db
 * @method static select(array|mixed $columns = ['*'])
 * @method static groupBy(string $string)
 * @method static get()
 * @mixin \Eloquent
 */
class Session extends Model
{
}
