<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Db\PostComment
 *
 * @method static create(array $data)
 * @property-read \App\Models\Db\Post $post
 * @mixin \Eloquent
 */
class PostComment extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'post_id',
        'author',
        'content',
    ];

    /**
     * Relation to post
     * @return BelongsTo
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }
}
