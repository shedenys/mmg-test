<?php

namespace App\Facades;

use App\Services\PageMeta\Contracts\PageMetaInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Class PageMeta
 * @package App\Facades
 */
class PageMeta extends Facade
{
    /**
     * Get the registered name of the component.
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return PageMetaInterface::class;
    }
}
