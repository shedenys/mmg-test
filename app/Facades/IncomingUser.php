<?php

namespace App\Facades;

use App\Services\User\Contracts\UserInterface;
use Illuminate\Support\Facades\Facade;

class IncomingUser extends Facade
{
    /**
     * Get the registered name of the component.
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return UserInterface::class;
    }
}
