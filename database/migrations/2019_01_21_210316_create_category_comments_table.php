<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration for Category Comments table
 * Class CreateCategoryCommentsTable
 */
class CreateCategoryCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('category_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->comment('Related Category ID');
            $table->string('author', 255)->comment('Author name');
            $table->text('content')->comment('Comment body');
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('category_comments');
    }
}
