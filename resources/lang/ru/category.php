<?php

return [
    'categories' => 'Категории',
    'create.title' => 'Создание категории',
    'index.no_categories' => 'Нет категорий',
    'index.add' => 'Добавить категорию',

    'form.name' => 'Название',
    'form.description' => 'Описание',

    'messages.create.success' => 'Категория успешно добавлена!',
    'messages.update.success' => 'Категория успешно обновлена!',
    'messages.delete.success' => 'Категория успешно удалена!',
];
