<?php

return [
    'index.title' => 'Выберите раздел',
    'categories' => 'Категории',
    'posts' => 'Посты',
    'go' => 'Перейти',
    'choose_a_section' => 'Выберите раздел',
];
