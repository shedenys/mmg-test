<?php

return [
    'required' => 'Поле :attribute обязательно для заполнения.',
    'file' => 'Поле :attribute должно быть успешно загруженным файлом.',
    'string'   => 'Поле должно быть строкой.',
    'max' => [
        'numeric' => 'Поле не может быть более :max.',
        'file'    => 'Размер файла в поле не может быть более :max Килобайт(а).',
    ],
    'attributes'  => include 'form/attributes.php'
];
