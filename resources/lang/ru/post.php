<?php

return [
    'posts' => 'Посты',
    'create.title' => 'Создание поста',
    'index.no_posts' => 'Нет постов',
    'index.add' => 'Добавить пост',
    'index.no_file' => 'Нет файла',
    'index.download' => 'Скачать',

    'messages.create.success' => 'Пост успешно добавлен!',
    'messages.update.success' => 'Пост успешно обновлен!',
    'messages.delete.success' => 'Пост успешно удален!',
];
