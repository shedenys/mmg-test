<?php

return [
    'name' => 'Название',
    'content' => 'Содержимое',
    'description' => 'Описание',
    'file' => 'Файл',
    'author' => 'Автор',
    'category_id' => 'Категория',
    'post_id' => 'Пост'
];
