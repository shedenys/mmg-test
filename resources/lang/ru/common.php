<?php

return [
    'alert.success' => 'Сохранено',

    'form.submit' => 'Сохранить',
    'form.add' => 'Добавить',
    'form.edit' => 'Редактировать',
    'form.delete' => 'Удалить',
    'form.show' => 'Просмотреть',
    'comments.no_comments' => 'Нет комментариев.',
    'comments.add' => 'Добавить комментарий',
    'comments.section_title' => 'Комментарии',
    'user_statistics' => 'Пользователей по браузерам',

    'messages.create.success.title' => 'Добавлено',
    'messages.update.success.title' => 'Обновлено',
    'messages.delete.success.title' => 'Удалено',
    'index.view' => 'Просмотреть',
    'link.home' => 'Главная'
];
