@extends('layout')

@section('main')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('home.categories') }}</h5>
                            <a href="{{ route('categories.index') }}" class="btn btn-primary">{{ __('home.go') }}</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('home.posts') }}</h5>
                            <a href="{{ route('posts.index') }}" class="btn btn-primary">{{ __('home.go') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
