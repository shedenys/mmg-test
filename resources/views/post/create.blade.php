@extends('layout')

@section('main')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('post.forms.edit', [
                        'url' => route('posts.store'),
                        'method' => 'post'
                    ])
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection