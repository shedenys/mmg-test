@extends('layout')

@section('main')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('common.partials.messages')
                </div>
            </div>
            @include('common.partials.inlineMenu')
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ $post->name }}</h1>
                    <p>{{ $post->created_at->format('d.m.Y, H:i') }}</p>
                    <div>
                        {!! $post->content !!}
                    </div>
                    <div class="mt-5">
                        <h4>{{ __('common.comments.section_title') }}</h4>
                        <div class="mb-5 mt-4" id="comments">
                            @if(count($comments))
                                @foreach($comments as $postComment)
                                    <div class="mb-4">
                                        <p><b>{{ $postComment->author }}</b></p>
                                        <p>{{ $postComment->content }}</p>
                                    </div>
                                @endforeach
                            @else
                                <div class="no-items">{{ __('common.comments.no_comments') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="mt-5">
                        <div class="mb-4">
                            <h4>{{ __('common.comments.add') }}</h4>
                        </div>
                        @include('post.forms.comment')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection