@extends('layout')

@section('main')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('common.partials.messages')
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="clearfix">
                        <div class="float-left">
                            <a href="{{ route('posts.create') }}" class="btn btn-primary">
                                {{ __('post.index.add') }}
                            </a>
                        </div>
                        <div class="float-right">
                            <a href="{{ route('categories.index') }}" class="btn btn-secondary">
                                {{ __('category.categories') }}
                            </a>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-primary" role="alert" @if(count($posts)) style="display: none;" @endif>
                        {{ __('post.index.no_posts') }}
                    </div>
                    @if(count($posts))
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('form/attributes.name') }}</th>
                                <th scope="col">{{ __('form/attributes.content') }}</th>
                                <th scope="col">{{ __('form/attributes.category_id') }}</th>
                                <th scope="col">{{ __('form/attributes.file') }}</th>
                                <th scope="col" width="20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->name }}</td>
                                    <td>
                                        {{ str_limit($post->content, \App\Models\Db\Post::CONTENT_PREVIEW_STR_LIMIT) }}
                                    </td>
                                    <td>{{ $post->category->name }}</td>
                                    <td>
                                        @if ($post->file)
                                            <a href="{{ route('posts.downloadFile', basename($post->file)) }}">
                                                {{ __('post.index.download') }}
                                            </a>
                                        @else
                                            {{ __('post.index.no_file') }}
                                        @endif
                                    </td>
                                    <td>
                                        <div>
                                            <a href="{{ route('posts.show', $post->id) }}" class="btn btn-primary">
                                                {{ __('common.form.show') }}
                                            </a>
                                        </div>
                                        <br>
                                        <div>
                                            <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary">
                                                {{ __('common.form.edit') }}
                                            </a>
                                        </div>
                                        <br>
                                        <div>
                                            <a href="{{ route('posts.delete', $post->id) }}" class="btn btn-danger">
                                                {{ __('common.form.delete') }}
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                    <br>
                    <div>
                        {!! $posts->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection