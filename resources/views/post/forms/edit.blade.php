{!! Form::open([
    'id' => 'ajax-form',
    'url' => $url,
    'method' => $method,
    'enctype' => 'multipart/form-data'
]) !!}
    <div class="form-group">
        {!! Form::label('name', __('form/attributes.name')); !!}
        {!! Form::text('name', $post->name, ['class' => 'form-control']); !!}
        <small id="nameDanger" class="form-text text-danger"></small>
    </div>
    <div class="form-group">
        {!! Form::label('content', __('form/attributes.content')); !!}
        {!! Form::textarea('content', $post->content, ['class' => 'form-control']); !!}
        <small id="contentDanger" class="form-text text-danger"></small>
    </div>
    <div class="form-group">
        {!! Form::label('category_id', __('form/attributes.category_id')); !!}
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control']); !!}
        <small id="category_idDanger" class="form-text text-danger"></small>
    </div>
    <div class="form-group">
        {!! Form::label('file', __('form/attributes.file')); !!}
        {!! Form::file('file'); !!}
        <small id="fileDanger" class="form-text text-danger"></small>
    </div>
    <div class="form-group">
        {!! Form::submit(__('common.form.submit'), ['class' => 'btn btn-primary']); !!}
    </div>
{!! Form::close(); !!}