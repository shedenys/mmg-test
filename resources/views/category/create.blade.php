@extends('layout')

@section('main')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('category.forms.edit', [
                        'url' => route('categories.store'),
                        'method' => 'post'
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection