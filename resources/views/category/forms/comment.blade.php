{!! Form::open([
    'id' => 'ajax-form',
    'url' => route('categories.storeComment'),
    'method' => 'post',
]) !!}
<div class="form-group">
    {!! Form::label('author', __('form/attributes.author')); !!}
    {!! Form::text('author', $comment->author, ['class' => 'form-control']); !!}
    <small id="authorDanger" class="form-text text-danger"></small>
</div>
<div class="form-group">
    {!! Form::label('content', __('form/attributes.content')); !!}
    {!! Form::textarea('content', $comment->content, ['class' => 'form-control']); !!}
    <small id="contentDanger" class="form-text text-danger"></small>
</div>
<div class="form-group">
    {!! Form::hidden('category_id', $category->id) !!}
    {!! Form::submit(__('common.form.add'), ['class' => 'btn btn-primary']); !!}
</div>
{!! Form::close(); !!}