{!! Form::open([
    'id' => 'ajax-form',
    'url' => $url,
    'method' => $method
]) !!}
    <div class="form-group">
        {!! Form::label('name', __('form/attributes.name')); !!}
        {!! Form::text('name', $category->name, ['class' => 'form-control']); !!}
        <small id="nameDanger" class="form-text text-danger"></small>
    </div>
    <div class="form-group">
        {!! Form::label('description', __('form/attributes.description')); !!}
        {!! Form::textarea('description', $category->description, ['class' => 'form-control']); !!}
        <small id="descriptionDanger" class="form-text text-danger"></small>
    </div>
    <div class="form-group">
        {!! Form::submit(__('common.form.submit'), ['class' => 'btn btn-primary']); !!}
    </div>
{!! Form::close(); !!}