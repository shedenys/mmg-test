@extends('layout')

@section('main')
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('common.partials.messages')
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="clearfix">
                        <div class="float-left">
                            <a href="{{ route('categories.create') }}" class="btn btn-primary">
                                {{ __('category.index.add') }}
                            </a>
                        </div>
                        <div class="float-right">
                            <a href="{{ route('posts.index') }}" class="btn btn-secondary">
                                {{ __('post.posts') }}
                            </a>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-primary" role="alert" @if(count($categories)) style="display: none;" @endif>
                        {{ __('category.index.no_categories') }}
                    </div>
                    @if(count($categories))
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('category.form.name') }}</th>
                                <th scope="col">{{ __('category.form.description') }}</th>
                                <th scope="col" width="20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->description }}</td>
                                    <td>
                                        <div>
                                            <a href="{{ route('categories.show', $category->id) }}" class="btn btn-primary">
                                                {{ __('common.form.show') }}
                                            </a>
                                        </div>
                                        <br>
                                        <div>
                                            <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">
                                                {{ __('common.form.edit') }}
                                            </a>
                                        </div>
                                        <br>
                                        <div>
                                            <a href="{{ route('categories.delete', $category->id) }}" class="btn btn-danger">
                                                {{ __('common.form.delete') }}
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                    <br>
                    <div>
                        {!! $categories->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection