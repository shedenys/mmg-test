<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ PageMeta::getTitle() }}</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="wrapper mt-2">
    <div class="container">
        <header>
            <div class="row">
                <div class="col-lg-12">
                    <div class="bs-component">
                        <div class="alert alert-dismissible alert-info">
                            <h4>{{ __('common.user_statistics') }}</h4>
                            <p>
                                @foreach(User::getBrowserStatistics() as $item)
                                    {{ $item->browser }}: {{ $item->count }}<br>
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <h1>{{ PageMeta::getTitle() }}</h1>
    </div>
</div>
<br>
@yield('main')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
@include('common.partials.ajax')
@section('scripts')
@show
</body>
</html>
