@if(Session::has('success'))
    <div class="bs-component">
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @if(Session::has('success-title'))
                <h4>{{ Session::get('success-title') }}</h4>
            @else
                <h4>{{ __('common.alert.success') }}</h4>
            @endif
            <p>{{ Session::get('success') }}</p>
        </div>
    </div>
    <br>
@endif