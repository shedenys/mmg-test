<div class="row">
    <div class="col-lg-12">
        <div class="clearfix">
            <div class="float-left">
                <a href="{{ route('home') }}" class="btn btn-primary">
                    {{ __('common.link.home') }}
                </a>
            </div>
            <div class="float-right btn-group" role="group">
                <a href="{{ route('categories.index') }}" class="btn btn-secondary">
                    {{ __('category.categories') }}
                </a>
                <a href="{{ route('posts.index') }}" class="btn btn-secondary">
                    {{ __('post.posts') }}
                </a>
            </div>
        </div>
        <br>
    </div>
</div>