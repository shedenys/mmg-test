@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            processData: false,
            contentType: false
        });

        $('#ajax-form').submit(function (e) {
            e.preventDefault();
            const current = $(this);
            const formData = new FormData($(this)[0]);
            sendRequest(current.attr('action'), current.attr('method'), formData);
        });

        function sendRequest(url, type, formData) {
            $.ajax({
                url: url,
                type: type,
                data: formData,
                success: function (data) {
                    if (data.redirect) {
                        window.location.replace(data.redirect);
                    }
                    if (data.comment) {
                        addComment(data.comment)
                    }
                    $('.text-danger').text('');
                },
                error: function (xhr) {
                    if (xhr.responseJSON !== undefined) {
                        if (xhr.responseJSON.errors !== undefined) {
                            for (var key in xhr.responseJSON.errors) {
                                $('#' + key + 'Danger').text(xhr.responseJSON.errors[key][0]);
                            }
                        }
                    }
                }
            });
        }

        function addComment(comment) {
            const comments = $('#comments');
            comments.append(
                "<div class=\"mb-4\">" +
                "<p><b>" + comment.author +"</b></p>" +
                "<p>" + comment.content + "</p>" +
                "</div>"
            );
            $('#ajax-form')[0].reset();
            comments.find('.no-items').remove();
        }
    </script>
@endsection