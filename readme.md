## Тестовое задание от MassMedia Group

Тестовое задание: https://docs.google.com/document/d/1I3hrkGXFXs1vqx49hghYNYuV84o-2QkyLaMJDXKrsAQ/edit

Системные требования:
PHP 7.2

Запуск приложения:
- composer update
- cp .env.example .env - и заполнить параметры подключения к БД
- cp .env .env.testing
- php artisan key:generate
- php artisan migrate

Запуск unit-тестов:
- vendor/bin/codecept run unit

На выполнение суммарно потрачено 30,03 часов.

Код оформлен по PSR-2.

При разработке использовался Vagrant.