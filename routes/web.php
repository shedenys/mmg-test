<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::resource('categories', 'CategoryController');
Route::get('/categories/{category}/delete', 'CategoryController@destroy')->name('categories.delete');
Route::resource('posts', 'PostController');
Route::post('/categories/comment', 'CategoryController@storeComment')->name('categories.storeComment');
Route::get('/posts/{post}/delete', 'PostController@destroy')->name('posts.delete');
Route::get('/posts/download-file/{filename}', 'PostController@downloadFile')->name('posts.downloadFile');
Route::post('/posts/comment', 'PostController@storeComment')->name('posts.storeComment');
