<?php

use App\Models\Db\Category;

class CategoryControllerTest extends \Codeception\Test\Unit
{
    /**
     * @var \AcceptanceTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testSomeFeature()
    {

    }

    /**
     * @return Category
     */
    private function createCategory(): Category
    {
        $category = factory(Category::class)->create();
        $category->name = 'name' . time();
        $category->description = 'description' . time();
        $category->save();

        return $category;


    }
}