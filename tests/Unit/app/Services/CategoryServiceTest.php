<?php

namespace Tests\Unit\App\Service;

use App\Models\Db\Category;
use App\Models\Db\CategoryComment;
use App\Services\Category\CategoryService;
use Faker\Factory;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Unit test for Category Service
 * Class CategoryServiceTest
 * @package Tests\Unit\App\Service
 */
class CategoryServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var CategoryService
     */
    protected $categoryService;

    protected function _before(): void
    {
        $this->categoryService = new CategoryService();
    }

    /**
     * Test get paginated lists of categories
     */
    public function testGetPaginated(): void
    {
        $faker = Factory::create();

        $this->tester->haveRecord(
            Category::class,
            [
                'name' => $faker->name(),
                'description' => $faker->sentence()
            ]
        );

        $this->tester->assertInstanceOf(
            LengthAwarePaginator::class,
            $this->categoryService->getPaginated()
        );
    }

    /**
     * Test category create
     */
    public function testCreate(): void
    {
        $faker = Factory::create();
        $name = $faker->name();
        $description = $faker->sentence();
        $this->categoryService->create([
            'name' => $name,
            'description' => $description
        ]);

        $this->tester->seeRecord(
            Category::class,
            ['name' => $name, 'description' => $description]
        );
    }

    /**
     * @return array
     */

    /**
     * Test get array of categories
     */
    public function testGetAllForSelect(): void
    {
        $faker = Factory::create();
        for ($i = 1; $i <= 2; $i++) {
            $this->tester->haveRecord(
                Category::class,
                [
                    'name' => $faker->name(),
                    'description' => $faker->sentence()
                ]
            );
        }
        $this->tester->assertCount(2, $this->categoryService->getAllForSelect());
    }

    /**
     * Test create comment for category
     */
    public function testCreateComment(): void
    {
        $faker = Factory::create();
        $categoryId = $this->tester->haveRecord(
            Category::class,
            [
                'name' => $faker->name(),
                'description' => $faker->sentence()
            ]
        )->{'id'};
        $author = $faker->name();
        $content = $faker->sentence();
        $comment = $this->categoryService->createComment([
            'category_id' => $categoryId,
            'author' => $author,
            'content' => $content
        ]);
        $this->tester->seeRecord(
            CategoryComment::class,
            [
                'category_id' => $categoryId,
                'author' => $author,
                'content' => $content
            ]
        );
        $this->tester->assertInstanceOf(CategoryComment::class,$comment);
    }
}
