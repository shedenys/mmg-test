<?php

namespace Tests\Unit\App\Service;

use App\Models\Db\Category;
use App\Models\Db\Post;
use App\Models\Db\PostComment;
use App\Services\Post\PostService;
use Faker\Factory;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Unit test for Post Service
 * Class PostServiceTest
 * @package Tests\Unit\App\Service
 */
class PostServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var PostService
     */
    protected $postService;

    protected $categoryId;

    protected function _before(): void
    {
        $faker = Factory::create();
        $this->postService = new PostService();
        $this->categoryId = $this->tester->haveRecord(
            Category::class,
            [
                'name' => $faker->name(),
                'description' => $faker->sentence()
            ]
        )->{'id'};
    }

    /**
     * Test get paginated lists of posts
     */
    public function testGetPaginated(): void
    {
        $faker = Factory::create();

        $this->tester->haveRecord(
            Post::class,
            [
                'category_id' => $this->categoryId,
                'name' => $faker->name(),
                'content' => $faker->text()
            ]
        );

        $this->tester->assertInstanceOf(
            LengthAwarePaginator::class,
            $this->postService->getPaginated()
        );
    }

    /**
     * Test post create
     */
    public function testCreate(): void
    {
        $faker = Factory::create();
        $name = $faker->name();
        $content = $faker->text();
        $this->postService->create([
            'category_id' => $this->categoryId,
            'name' => $name,
            'content' => $content
        ]);

        $this->tester->seeRecord(
            Post::class,
            ['name' => $name, 'content' => $content]
        );
    }

    /**
     * Test create comment for post
     */
    public function testCreateComment(): void
    {
        $faker = Factory::create();
        $postId = $this->tester->haveRecord(
            Post::class,
            [
                'category_id' => $this->categoryId,
                'name' => $faker->name(),
                'content' => $faker->text()
            ]
        )->{'id'};
        $author = $faker->name();
        $content = $faker->sentence();
        $comment = $this->postService->createComment([
            'post_id' => $postId,
            'author' => $author,
            'content' => $content
        ]);
        $this->tester->seeRecord(
            PostComment::class,
            [
                'post_id' => $postId,
                'author' => $author,
                'content' => $content
            ]
        );
        $this->tester->assertInstanceOf(PostComment::class,$comment);
    }
}
